import time
from pprint import pprint
from functools import wraps
from typing import Callable, Any, Dict


def store(payload: Dict):
    pprint(payload)


def timeit(policy: Callable):
    @wraps(policy)
    def timed(*args, **kw):
        start_time = time.time()
        result = policy(*args, **kw)
        end_time = time.time()

        execution = {
            'start_time': start_time,
            'end_time': end_time
        }

        return execution, result

    return timed


def gather_data(policy: Callable) -> Any:
    @wraps(policy)
    def with_data_gathered(*args, **kwargs):
        execution, decision = timeit(policy)(*args, **kwargs)

        try:
            last_execution = with_data_gathered.last_execution_time
        except AttributeError:
            with_data_gathered.last_execution_time = last_execution = {}

        payload = {
            'context': {
                'args': args,
                'kwargs': kwargs
            },
            'decision': decision,
            'execution': execution,
            'last_execution': last_execution
        }

        with_data_gathered.last_execution_time = execution

        store(payload)

        return decision
    return with_data_gathered
