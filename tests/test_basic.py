import pytest
from policydata import gather_data


def test_basic():
    def policy(value):
        return value > 0

    policy_with_data = gather_data(policy)

    for value in range(-10, 10, 1):
        assert policy(value) == policy_with_data(value)
