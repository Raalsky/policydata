from setuptools import setup
from setuptools import find_packages

setup(
    name='policydata',
    packages=find_packages(),
    version='0.0.1',
    author='Rafal Jankowski',
    author_email='jan.rafalkowski+policydata@gmail.com',
    license='MIT License',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        ],
    url='https://gitlab.com/raalsky/policydata'
)
